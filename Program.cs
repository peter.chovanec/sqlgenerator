﻿using System;
using System.Diagnostics;
using System.Diagnostics.Metrics;
using System.Runtime.InteropServices;
using System.Windows.Forms;


namespace SqlGenerator
{
    internal static class Program
    {
        private const int WH_KEYBOARD_LL = 13;
        private const int WM_KEYDOWN = 0x0100;

        private static IntPtr hookId = IntPtr.Zero;
        private static LowLevelKeyboardProc keyboardProc;

        [STAThread]
        static void Main()
        {
            Process ssmsProcess = GetSSMSProcess();

            if (ssmsProcess != null)
            {
                using (ProcessModule module = ssmsProcess.MainModule)
                {
                    keyboardProc = KeyboardHookCallback;
                    hookId = SetKeyboardHook(keyboardProc);

                    Application.Run();

                    UnhookWindowsHookEx(hookId);
                }
            }
            else
            {
                MessageBox.Show("Aplikácia Microsoft SQL Server Management Studio nie je spustená.");
            }
        }

        private static Process GetSSMSProcess()
        {
            Process[] processes = Process.GetProcessesByName("Ssms");

            if (processes.Length > 0)
            {
                return processes[0];
            }
            else
            {
                return null;
            }
        }

        private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);

        private static IntPtr SetKeyboardHook(LowLevelKeyboardProc proc)
        {
            using (ProcessModule module = Process.GetCurrentProcess().MainModule)
            {
                return SetWindowsHookEx(WH_KEYBOARD_LL, proc, GetModuleHandle(module.ModuleName), 0);
            }
        }

        private static bool isSendingKeys = false;
        private static string[] strings = { "SELECT title\nFROM film\n",
                                            "WHERE film_id NOT IN (\n   SELECT i1.film_id\n   FROM rental r1 JOIN inventory i1 ON r1.inventory_id = i1.inventory_id\n)",
                                            "   WHERE EXISTS (\n      SELECT *\n      FROM rental r2 JOIN inventory i2 ON r2.inventory_id = i2.inventory_id\n   )   \b\b\b",
                                            "      WHERE r1.customer_id = r2.customer_id",
                                            " AND i1.film_id != i2.film_id",
                                            "AND film_id IN (\n   SELECT film_id\n   FROM inventory JOIN rental ON inventory.inventory_id = rental.inventory_id\n)",
                                            " DISTINCT",
                                            "AND",
                                            "WHERE film.film_id = film_actor.film_id AND actor_id = 1)",
                                            "WHERE film.film_id = film_actor.film_id AND actor_id = 1",
                                            "LEFT JOIN customer ON rental.customer_id = customer.customer_id",
                                            "LEFT JOIN address ON customer.address_id = address.address_id",
                                            "film.film_id, film.title, COUNT(DISTINCT address.city_id)",
                                            "GROUP BY film.film_id, film.title"};
        private static int order = 0;

        private static void PrintText()
        {
            isSendingKeys = true;
            foreach (char c in strings[order++])
            {
                if (c == '\n')  // Pridanie escape sekvencie pre nový riadok
                    SendKeys.Send("{ENTER}");
                if (c == '\b')  // Klávesa Delete
                    SendKeys.Send("{DELETE}");
                if ((c == '(') || (c == ')') || (c == '%'))
                    SendKeys.Send("{" + c.ToString() + "}");
                else
                    SendKeys.Send(c.ToString());
                Thread.Sleep(80); // Spomalenie odosielania znaku na 200 milisekúnd
            }
            isSendingKeys = false;
        }

        private static IntPtr KeyboardHookCallback(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode >= 0 && wParam == (IntPtr)WM_KEYDOWN)
            {
                int vkCode = Marshal.ReadInt32(lParam);
                Keys key = (Keys)vkCode;

                if (IsSSMSActive())
                {
                    if (!isSendingKeys)
                    {
                        if (key == Keys.F12)
                        {
                            SendKeys.Flush();
                            PrintText();
                            return (IntPtr)1;
                        }
                    }

                }
            }

            return CallNextHookEx(hookId, nCode, wParam, lParam);
        }

        private static bool IsSSMSActive()
        {
            IntPtr activeWindowHandle = GetActiveWindow();
            IntPtr ssmsMainWindowHandle = GetSSMSMainWindowHandle();

            return (activeWindowHandle == ssmsMainWindowHandle);
        }

        private static IntPtr GetActiveWindow()
        {
            return GetForegroundWindow();
        }

        private static IntPtr GetSSMSMainWindowHandle()
        {
            Process ssmsProcess = GetSSMSProcess();
            return ssmsProcess.MainWindowHandle;
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetForegroundWindow();
    }
}